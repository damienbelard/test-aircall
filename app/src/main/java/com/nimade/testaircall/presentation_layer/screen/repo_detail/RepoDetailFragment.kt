package com.nimade.testaircall.presentation_layer.screen.repo_detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.navArgs
import com.nimade.testaircall.R
import com.nimade.testaircall.databinding.FragmentRepoDetailBinding
import com.nimade.testaircall.presentation_layer.MainActivity
import com.nimade.testaircall.utils.ChromeCustomTabsUtils
import com.nimade.testaircall.utils.StringUtils
import javax.inject.Inject

class RepoDetailFragment : Fragment(R.layout.fragment_repo_detail) {

    private val args: RepoDetailFragmentArgs by navArgs()

    @Inject
    lateinit var navController: NavController

    @Inject
    lateinit var chromeCustomTabsUtils: ChromeCustomTabsUtils

    @Inject
    lateinit var stringUtils: StringUtils

    private var _binding: FragmentRepoDetailBinding? = null
    private val binding
        get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = FragmentRepoDetailBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).mainActivityComponent.inject(this)

        binding.apply {
            reposDetailNavigationToolbar.apply {
                setOnBackArrowClick {
                    navController.navigateUp()
                }
            }
            reposDetailAvatar.setImage(args.selectedRepo.ownerAvatarUrl)
            reposDetailOwnerName.text = args.selectedRepo.ownerName
            reposDetailTitle.text = args.selectedRepo.repoTitle
            reposDetailDescription.text = args.selectedRepo.repoDescription
            if (args.selectedRepo.repoHomePage != null) {
                reposDetailRepoHomePage.apply {
                    text = args.selectedRepo.repoHomePage
                    setOnClickListener {
                        chromeCustomTabsUtils.openUrl(args.selectedRepo.repoHomePage!!)
                    }
                }
            } else {
                reposDetailRepoHomePage.visibility = View.GONE
                reposDetailLinkIcon.visibility = View.GONE
            }
            reposDetailRepoStarAmount.text = stringUtils.parseAmount(
                args.selectedRepo.repoStarAmount,
                R.string.star_amount_placeholder,
                R.string.star_amount_placeholder_no_k
            )
            reposDetailRepoForkAmount.text = stringUtils.parseAmount(
                args.selectedRepo.repoForkAmount,
                R.string.fork_amount_placeholder,
                R.string.fork_amount_placeholder_no_k
            )
            reposDetailIssueAmount.text = args.selectedRepo.repoOpenIssues.toString()
            reposDetailWatchersAmount.text = stringUtils.parseAmount(
                args.selectedRepo.repoWatchersAmount,
                R.string.watchers_amount_placeholder,
                R.string.watchers_amount_placeholder_no_K
            )
        }
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

}