package com.nimade.testaircall.domain_layer.model

data class AuthResponseModel(
    val token: String
)
