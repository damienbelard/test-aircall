package com.nimade.testaircall.data_layer.model

import com.google.gson.annotations.SerializedName

data class AuthLoginAccessTokenRequest(
    @SerializedName("client_id")
    val clientId: String,
    @SerializedName("client_secret")
    val clientSecret: String,
    @SerializedName("code")
    val code: String
)