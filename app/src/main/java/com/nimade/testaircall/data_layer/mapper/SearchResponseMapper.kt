package com.nimade.testaircall.data_layer.mapper

import com.nimade.testaircall.data_layer.model.Item
import com.nimade.testaircall.data_layer.model.SearchReposResponse
import com.nimade.testaircall.di.MainActivityScope
import com.nimade.testaircall.domain_layer.model.RepoModel
import javax.inject.Inject

@MainActivityScope
class SearchResponseMapper @Inject constructor() {
    fun map(item: Item): RepoModel = RepoModel(
        ownerAvatarUrl = item.owner.avatarUrl,
        ownerName = item.owner.login,
        repoTitle = item.name,
        repoDescription = item.description ?: "",
        repoStarAmount = item.stargazersCount,
        repoId = item.id,
        repoForkAmount = item.forksCount,
        repoOpenIssues = item.openIssuesCount,
        repoWatchersAmount = item.watchersCount,
        repoHomePage = item.homepage
    )

    fun map(searchReposResponse: SearchReposResponse): List<RepoModel> = searchReposResponse.items.map {
        map(it)
    }
}