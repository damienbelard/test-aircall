package com.nimade.testaircall.retrofit

import com.nimade.testaircall.data_layer.model.SearchReposResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface GitApiService {
    @Headers("Accept: application/json")
    @GET("/search/repositories?q=topic:kotlin&sort=stars")
    suspend fun searchRepos(@Query("page") page: Int): SearchReposResponse
}