package com.nimade.testaircall.domain_layer

import com.nimade.testaircall.data_layer.model.SearchReposResponse
import com.nimade.testaircall.domain_layer.model.AuthResponseModel
import com.nimade.testaircall.domain_layer.model.RepoModel
import javax.inject.Inject

class GitUseCase @Inject constructor(
    private val gitRepository: GitRepository
) {
    suspend fun getToken(code: String): AuthResponseModel = gitRepository.getToken(code)
    suspend fun getKotlinRepos(page: Int): List<RepoModel> = gitRepository.getTopkotlinRepos(page)
}