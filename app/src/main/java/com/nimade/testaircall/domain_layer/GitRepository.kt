package com.nimade.testaircall.domain_layer

import com.nimade.testaircall.data_layer.model.SearchReposResponse
import com.nimade.testaircall.domain_layer.model.AuthResponseModel
import com.nimade.testaircall.domain_layer.model.RepoModel

interface GitRepository {
    suspend fun getToken(code: String): AuthResponseModel
    suspend fun getTopkotlinRepos(page: Int): List<RepoModel>
}