package com.nimade.testaircall.domain_layer.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RepoModel(
    val ownerAvatarUrl: String,
    val ownerName: String,
    val repoTitle: String,
    val repoDescription: String,
    val repoStarAmount: Int,
    val repoId: Int,
    val repoWatchersAmount: Int,
    val repoForkAmount: Int,
    val repoOpenIssues: Int,
    val repoHomePage: String?
): Parcelable
