package com.nimade.testaircall.presentation_layer.screen.auth

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.nimade.testaircall.R
import com.nimade.testaircall.databinding.FragmentAuthLandingPageBinding
import com.nimade.testaircall.presentation_layer.MainActivity
import com.nimade.testaircall.presentation_layer.ui_components.NotificationPanel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject


class AuthLandingPageFragment : Fragment(R.layout.fragment_auth_landing_page) {

    @ExperimentalCoroutinesApi
    private var notificationPanel: NotificationPanel? = null

    private val args: AuthLandingPageFragmentArgs by navArgs()

    private var _binding: FragmentAuthLandingPageBinding? = null
    private val binding
        get() = _binding!!

    @FlowPreview
    @ExperimentalCoroutinesApi
    @Inject
    lateinit var vmFactory: AuthLanfingPageViewmodel.Factory

    @FlowPreview
    @ExperimentalCoroutinesApi
    private lateinit var model: AuthLanfingPageViewmodel

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = FragmentAuthLandingPageBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).mainActivityComponent.inject(this)

        model = ViewModelProvider(this, vmFactory)[AuthLanfingPageViewmodel::class.java]
        model.state.observe(viewLifecycleOwner, Observer { state ->
            renderState(state)
        })

        binding.authLandingPageButton.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/login/oauth/authorize?client_id=0b960d7144f26fd7db52"))
            startActivity(browserIntent)
        }

        if (args.code != "no_code") {
            model.onLoginCodeReceived(args.code)
        }
    }

    @ExperimentalCoroutinesApi
    private fun renderState(state: AuthLandingPageState) {
        binding.authLandingPageButton.isEnabled = state.networkAvailable
        if (state.isLoginFailure){
            if (notificationPanel == null) notificationPanel =
                NotificationPanel.makeModal(requireContext())
            notificationPanel!!
                .setPanel(
                    R.string.generic_error,
                    R.color.reddish,
                    false
                )
                .showPanel(
                    binding.root,
                    ConstraintLayout.LayoutParams(
                        ConstraintLayout.LayoutParams.MATCH_PARENT,
                        ConstraintLayout.LayoutParams.WRAP_CONTENT
                    ),
                    9f
                )
        } else notificationPanel?.hidePanel()
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}