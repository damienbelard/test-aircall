package com.nimade.testaircall.data_layer.mapper

import com.nimade.testaircall.data_layer.model.AuthloginAccessTokenResponse
import com.nimade.testaircall.di.MainActivityScope
import com.nimade.testaircall.domain_layer.model.AuthResponseModel
import javax.inject.Inject

@MainActivityScope
class AuthResponseModelMapper @Inject constructor() {
    fun mapAuthLoginAccessTokenResponseToAuthResponseModel(authloginAccessTokenResponse: AuthloginAccessTokenResponse) =
        AuthResponseModel(authloginAccessTokenResponse.token)
}