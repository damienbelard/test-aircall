package com.nimade.testaircall.retrofit

import com.nimade.testaircall.data_layer.model.AuthLoginAccessTokenRequest
import com.nimade.testaircall.data_layer.model.AuthloginAccessTokenResponse
import com.nimade.testaircall.data_layer.model.SearchReposResponse
import retrofit2.http.*

interface GitLoginService {
    @Headers("Accept: application/json")
    @POST("login/oauth/access_token")
    suspend fun getAccessToken(@Body authLoginAccessTokenRequest: AuthLoginAccessTokenRequest): AuthloginAccessTokenResponse
}