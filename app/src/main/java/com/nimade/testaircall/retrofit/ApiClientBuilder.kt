package com.nimade.testaircall.retrofit

import com.google.gson.GsonBuilder
import com.nimade.testaircall.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClientBuilder {

    private val retrofitLogin = Retrofit.Builder()
        .baseUrl(BuildConfig.LOGIN_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()

    private val retrofitApi = Retrofit.Builder()
        .baseUrl(BuildConfig.API_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()

    fun <T> buildLoginService(apiClass: Class<T>): T {
        return retrofitLogin.create(apiClass)
    }

    fun <T> buildApiService(apiClass: Class<T>): T {
        return retrofitApi.create(apiClass)
    }
}