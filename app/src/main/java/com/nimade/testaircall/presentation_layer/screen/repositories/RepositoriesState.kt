package com.nimade.testaircall.presentation_layer.screen.repositories

import com.nimade.testaircall.presentation_layer.model.RepoItem

data class RepositoriesState(
    val isLoading: Boolean = false,
    val repos: List<RepoItem> = emptyList(),
    val isFailure: Boolean = false
)
