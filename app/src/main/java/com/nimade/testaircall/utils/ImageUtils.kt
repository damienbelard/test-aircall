package com.nimade.testaircall.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.media.ThumbnailUtils
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.nimade.testaircall.presentation_layer.ui_components.CircularImage
import kotlin.math.max
import kotlin.math.min


object ImageUtils {
    val SQUARE_RATIO = Pair(1, 1)
    val RECTANGLE_RATIO = Pair(16, 9)

    private var currentRatio = SQUARE_RATIO

    fun bitmapToDrawable(bitmap: Bitmap, resources: Resources): BitmapDrawable {
        return BitmapDrawable(resources, bitmap)
    }

    fun drawableToBitmap(drawable: Drawable): Bitmap {
        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }
        val bitmap: Bitmap = if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            Bitmap.createBitmap(
                1,
                1,
                Bitmap.Config.ARGB_8888
            ) // Single color bitmap will be created of 1x1 pixel
        } else {
            Bitmap.createBitmap(
                drawable.intrinsicWidth,
                drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
        }
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    fun getGradientDrawable(
        startColor: Int = Color.TRANSPARENT,
        endColor: Int = Color.TRANSPARENT,
        orientation: GradientDrawable.Orientation = GradientDrawable.Orientation.TOP_BOTTOM
    ) = GradientDrawable(orientation, IntArray(2) {
        when (it) {
            0 -> startColor
            1 -> endColor
            else -> Color.TRANSPARENT
        }
    })

    fun getBackgroundDrawable(
        gradientDrawableShape: Int,
        cornerRadiusInPx: Float,
        color: Int,
        strokeSize: Int = 0,
        strokeColor: Int? = null
    ) = getBackgroundDrawableWithVariableCornerRadius(
        gradientDrawableShape,
        cornerRadiusInPx,
        cornerRadiusInPx,
        cornerRadiusInPx,
        cornerRadiusInPx,
        color,
        strokeSize,
        strokeColor
    )

    fun getBackgroundDrawableWithVariableCornerRadius(
        gradientDrawableShape: Int,
        topLeftCornerRadiusInPx: Float = 0f,
        topRightCornerRadiusInPx: Float = 0f,
        bottomLeftCornerRadiusInPx: Float = 0f,
        bottomRightCornerRadiusInPx: Float = 0f,
        color: Int,
        strokeSize: Int = 0,
        strokeColor: Int? = null
    ) = GradientDrawable().apply {
        shape = gradientDrawableShape
        cornerRadii = FloatArray(8).apply {
            set(0, topLeftCornerRadiusInPx)
            set(1, topLeftCornerRadiusInPx)
            set(2, topRightCornerRadiusInPx)
            set(3, topRightCornerRadiusInPx)
            set(4, bottomRightCornerRadiusInPx)
            set(5, bottomRightCornerRadiusInPx)
            set(6, bottomLeftCornerRadiusInPx)
            set(7, bottomLeftCornerRadiusInPx)
        }
        setColor(color)
        if (strokeSize > 0 && strokeColor != null) {
            setStroke(strokeSize, strokeColor)
        }
    }


    fun getRoundedDrawable(bitmap: Bitmap, resources: Resources): RoundedBitmapDrawable {
        var finalBitmap = bitmap
        if (bitmap.height != bitmap.width) {
            val size = min(bitmap.height, bitmap.width)
            val squareBitmap = ThumbnailUtils.extractThumbnail(bitmap, size, size)
            finalBitmap = squareBitmap
        }

        val dr = RoundedBitmapDrawableFactory.create(resources, finalBitmap)
        dr.cornerRadius = (max(bitmap.height, bitmap.width)).toFloat()
        return dr
    }

    fun getRoundedDrawable(drawableResource: Int, resources: Resources): RoundedBitmapDrawable {
        val bitmap = BitmapFactory.decodeResource(resources, drawableResource)
        return getRoundedDrawable(bitmap, resources)
    }

    fun downloadAndSetRoundImage(url: String, context: Context, circularImage: CircularImage) {
        Glide.with(context)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    circularImage.setImageDrawable(getRoundedDrawable(resource, context.resources))
                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })
    }

    fun downloadAndSetImage(imageView: ImageView, url: String) {
        Glide.with(imageView)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    imageView.setImageBitmap(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })
    }

    fun downloadAndSetImage(circularImage: CircularImage, url: String) {
        Glide.with(circularImage)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    circularImage.setImage(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })
    }

    fun downloadAndGetImageAsBitmap(
        url: String,
        context: Context,
        getBitmap: (bitmap: Bitmap) -> Unit
    ) {
        Glide.with(context)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    getBitmap(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })
    }
}