package com.nimade.testaircall

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.GsonBuilder
import com.nimade.testaircall.data_layer.model.AuthLoginAccessTokenRequest
import com.nimade.testaircall.retrofit.GitApiService
import com.nimade.testaircall.retrofit.GitLoginService
import com.nimade.testaircall.utils.JsonUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

class NetworkRequestUnitTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val mockWebServer = MockWebServer()
    private lateinit var gitLoginService: GitLoginService
    private lateinit var gitApiService: GitApiService

    @Before
    fun setup() {
        mockWebServer.start()
        val url = mockWebServer.url("/")
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()

        gitLoginService = retrofit.create(GitLoginService::class.java)
        gitApiService = retrofit.create(GitApiService::class.java)
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test get access token`() {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(
                JsonUtils.getJsonFromFile(
                    javaClass.classLoader?.getResource("login_api_access_token_response_ok.json")
                        ?.openStream()!!
                )!!
            )
        mockWebServer.enqueue(response)
        runBlocking {
            val tokenModel = gitLoginService.getAccessToken(AuthLoginAccessTokenRequest(
                clientId = "id",
                clientSecret = "secret",
                code = "code"
            ))
            println(tokenModel.toString())
            assertEquals("e72e16c7e42f292c6912e7710c838347ae178b4a", tokenModel.token)
            assertEquals("repo,gist", tokenModel.scope)
            assertEquals("bearer", tokenModel.tokenType)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test get repos`() {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(
                JsonUtils.getJsonFromFile(
                    javaClass.classLoader?.getResource("data_api_sear_repo_response_ok.json")
                        ?.openStream()!!
                )!!
            )
        mockWebServer.enqueue(response)
        runBlocking {
            val repos = gitApiService.searchRepos(1)
            println(repos.toString())
            assertEquals(22709, repos.totalCount)
            assertEquals(10, repos.items.size)
            assertEquals(14098069, repos.items.first().id)
        }
    }
}