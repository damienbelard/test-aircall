package com.nimade.testaircall.presentation_layer.model

import com.nimade.testaircall.R
import com.nimade.testaircall.utils.ImageUtils
import com.nimade.testaircall.utils.StringUtils
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_repo.*

class RepoItem(
    val ownerAvatarUrl: String,
    val ownerName: String,
    val repoTitle: String,
    val repoDescription: String,
    val repoStarAmount: String,
    val repoId: Int
) : Item() {
    override fun getLayout(): Int = R.layout.item_repo

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemRepoOwnerName.text = ownerName
        viewHolder.itemRepoTitle.text = repoTitle
        viewHolder.itemRepoDescription.text = repoDescription
        viewHolder.itemRepoStar.setImage(R.drawable.star)
        viewHolder.itemRepoDStarAmount.text = repoStarAmount
        ImageUtils.downloadAndSetImage(viewHolder.itemRepoAvatar, ownerAvatarUrl)
    }
}