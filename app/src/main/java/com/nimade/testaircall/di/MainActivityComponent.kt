package com.nimade.testaircall.di

import android.content.Context
import androidx.navigation.NavController
import com.nimade.testaircall.presentation_layer.screen.auth.AuthLandingPageFragment
import com.nimade.testaircall.presentation_layer.MainActivity
import com.nimade.testaircall.presentation_layer.screen.repo_detail.RepoDetailFragment
import com.nimade.testaircall.presentation_layer.screen.repositories.RepositoriesFragment
import dagger.BindsInstance
import dagger.Component

@Component(modules = [MainActivityModule::class])
@MainActivityScope
interface MainActivityComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(authLandingPageFragment: AuthLandingPageFragment)
    fun inject(repositoriesFragment: RepositoriesFragment)
    fun inject(repoDetailFragment: RepoDetailFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder

        @BindsInstance
        fun navController(navController: NavController): Builder

        fun build(): MainActivityComponent
    }
}