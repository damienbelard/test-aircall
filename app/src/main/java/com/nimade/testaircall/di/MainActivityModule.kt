package com.nimade.testaircall.di

import com.nimade.testaircall.data_layer.GitRepositoryImpl
import com.nimade.testaircall.data_layer.datasource.GitApiDataSource
import com.nimade.testaircall.data_layer.mapper.AuthResponseModelMapper
import com.nimade.testaircall.data_layer.mapper.SearchResponseMapper
import com.nimade.testaircall.domain_layer.GitRepository
import com.nimade.testaircall.retrofit.ApiClientBuilder
import com.nimade.testaircall.retrofit.GitApiDataSourceImpl
import com.nimade.testaircall.retrofit.GitApiService
import com.nimade.testaircall.retrofit.GitLoginService
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    @MainActivityScope
    fun provideGitLoginService(): GitLoginService = ApiClientBuilder.buildLoginService(GitLoginService::class.java)
    @Provides
    @MainActivityScope
    fun provideGitApiService(): GitApiService = ApiClientBuilder.buildApiService(GitApiService::class.java)

    @Provides
    @MainActivityScope
    fun provideGitApiDataSource(
        gitLoginService: GitLoginService,
        gitApiService: GitApiService
    ): GitApiDataSource = GitApiDataSourceImpl(
        gitLoginService,
        gitApiService
    )

    @Provides
    @MainActivityScope
    fun provideGitRepository(
        gitApiDataSource: GitApiDataSource,
        authResponseModelMapper: AuthResponseModelMapper,
        searchResponseMapper: SearchResponseMapper,
    ): GitRepository = GitRepositoryImpl(
        authResponseModelMapper,
        searchResponseMapper,
        gitApiDataSource
    )
}