package com.nimade.testaircall.data_layer

import com.nimade.testaircall.data_layer.datasource.GitApiDataSource
import com.nimade.testaircall.data_layer.mapper.AuthResponseModelMapper
import com.nimade.testaircall.data_layer.mapper.SearchResponseMapper
import com.nimade.testaircall.domain_layer.GitRepository
import com.nimade.testaircall.domain_layer.model.AuthResponseModel

class GitRepositoryImpl(
    private val tokenModelMapper: AuthResponseModelMapper,
    private val searchResponseMapper: SearchResponseMapper,
    private val gitApiDataSource: GitApiDataSource
) : GitRepository {
    override suspend fun getToken(code: String): AuthResponseModel =
        tokenModelMapper.mapAuthLoginAccessTokenResponseToAuthResponseModel(
            gitApiDataSource.getAccessToken(code)
        )

    override suspend fun getTopkotlinRepos(page: Int) =
        searchResponseMapper.map(gitApiDataSource.getTopKotlinRepos(page))
}