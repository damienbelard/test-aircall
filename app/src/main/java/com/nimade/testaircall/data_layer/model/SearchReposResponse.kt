package com.nimade.testaircall.data_layer.model


import com.google.gson.annotations.SerializedName

data class SearchReposResponse(
    @SerializedName("incomplete_results")
    val incompleteResults: Boolean,
    @SerializedName("items")
    val items: List<Item>,
    @SerializedName("total_count")
    val totalCount: Int
)