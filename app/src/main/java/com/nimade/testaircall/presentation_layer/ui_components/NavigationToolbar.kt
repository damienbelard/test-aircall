package com.nimade.testaircall.presentation_layer.ui_components

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.nimade.testaircall.R
import com.nimade.testaircall.databinding.NavigationToolbarBinding

class NavigationToolbar(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {

    private var iconDrawable: Drawable?

    private val binding: NavigationToolbarBinding =
        NavigationToolbarBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        val attributes = context.obtainStyledAttributes(
            attributeSet,
            R.styleable.NavigationToolbar
        )

        binding.navigationToolbarTitle.text =
            attributes.getText(R.styleable.NavigationToolbar_title)
        iconDrawable = attributes.getDrawable(R.styleable.NavigationToolbar_icon)

        if (!attributes.getBoolean(R.styleable.NavigationToolbar_back_arrow, true)) {
            binding.navigationToolbarBackArrow.visibility = View.GONE
        }

        if (iconDrawable != null) {
            binding.navigationToolbarIcon.setImageDrawable(iconDrawable)
            binding.navigationToolbarIcon.visibility = View.VISIBLE
        } else binding.navigationToolbarIcon.visibility = View.GONE

        attributes.recycle()
    }

    fun setOnIconClick(onIconClick: () -> Unit): NavigationToolbar {
        binding.navigationToolbarIcon.setOnClickListener {
            onIconClick()
        }
        return this
    }

    fun setOnBackArrowClick(onBackArrowClick: () -> Unit): NavigationToolbar {
        binding.navigationToolbarBackArrow.setOnClickListener {
            onBackArrowClick()
        }
        return this
    }

    fun setIconVisible() {
        binding.navigationToolbarIcon.visibility = View.VISIBLE
    }

    fun setIconInvisible() {
        binding.navigationToolbarIcon.visibility = View.GONE
    }

    fun setTitle(title: String) {
        this.binding.navigationToolbarTitle.text = title
    }
}