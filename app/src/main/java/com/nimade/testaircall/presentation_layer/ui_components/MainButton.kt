package com.nimade.testaircall.presentation_layer.ui_components

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.button.MaterialButton
import com.nimade.testaircall.R
import com.nimade.testaircall.utils.DpUtils


class MainButton(context: Context, attributeSet: AttributeSet) :
    MaterialButton(context, attributeSet) {
    init {
        val attributes = context.obtainStyledAttributes(
            attributeSet,
            R.styleable.MainButton
        )
        backgroundTintList = ColorStateList.valueOf(
            attributes.getColor(
                R.styleable.MainButton_main_button_color, ContextCompat.getColor(context, R.color.accent_color)
            )
        )

        text = attributes.getText(R.styleable.MainButton_main_button_text)
        //typeface = ResourcesCompat.getFont(context, R.font.pt_sans_bold)
        textSize = 16f
        isAllCaps = false
        val padding = if (attributes.getBoolean(
                R.styleable.MainButton_main_button_padding,
                true
            )
        ) DpUtils.convertDpToPx(context, 25f) else 0
        setPadding(
            0,
            padding, 0,
            padding
        )
        cornerRadius = DpUtils.convertDpToPx(context, 30f)
        elevation = DpUtils.convertDpToPx(context, 0f).toFloat()
        letterSpacing = 0.02f
        attributes.recycle()
    }

    override fun setEnabled(enabled: Boolean) {
        alpha = if (!enabled) {
            0.5f
        } else {
            1.0f
        }
        super.setEnabled(enabled)
    }
}