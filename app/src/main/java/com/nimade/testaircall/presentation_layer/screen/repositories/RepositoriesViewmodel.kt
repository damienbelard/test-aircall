package com.nimade.testaircall.presentation_layer.screen.repositories

import androidx.lifecycle.*
import androidx.navigation.NavController
import com.nimade.testaircall.R
import com.nimade.testaircall.domain_layer.GitUseCase
import com.nimade.testaircall.domain_layer.model.RepoModel
import com.nimade.testaircall.presentation_layer.model.RepoItem
import com.nimade.testaircall.presentation_layer.streams.CoreStreams
import com.nimade.testaircall.utils.StringUtils
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class RepositoriesViewmodel(
    private val gitUseCase: GitUseCase,
    private val navController: NavController,
    private val stringUtils: StringUtils
) : ViewModel() {
    /* Factory for creating FeatureViewModel instances */
    class Factory @Inject constructor(
        private val gifUseCase: GitUseCase,
        private val navController: NavController,
        private val stringUtils: StringUtils
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return RepositoriesViewmodel(
                gifUseCase,
                navController,
                stringUtils
            ) as T
        }
    }

    private val _state: MutableLiveData<RepositoriesState> = MutableLiveData()
    val state: LiveData<RepositoriesState>
        get() = _state

    private var currentPage: Int = 0
    private var repoItems: MutableList<RepoItem> = mutableListOf()
    private var repoModels: MutableList<RepoModel> = mutableListOf()

    init {
        _state.value = RepositoriesState()
        CoreStreams.isNetworkAvailable
            .asFlow()
            .flowOn(Dispatchers.Default)
            .distinctUntilChanged()
            .onEach { available ->
                if (available != null && available == false) {
                   viewModelScope.cancel()
                    _state.value = _state.value!!.copy(
                        isLoading = false
                    )
                }
            }
            .flowOn(Dispatchers.Main)
            .launchIn(GlobalScope)
        searchForRepo()
    }

    private fun searchForRepo(){
        _state.value = _state.value!!.copy(
            isLoading = true,
            isFailure = false
        )
        viewModelScope.launch(Dispatchers.Main) {
            try {
                val repoModels = withContext(Dispatchers.IO) {
                    return@withContext gitUseCase.getKotlinRepos(currentPage)
                }

                this@RepositoriesViewmodel.repoModels.addAll(repoModels)

                val items = repoModels.map {
                    RepoItem(
                        ownerAvatarUrl = it.ownerAvatarUrl,
                        ownerName = it.ownerName,
                        repoTitle = it.repoTitle,
                        repoDescription = it.repoDescription,
                        repoStarAmount = stringUtils.parseAmount(it.repoStarAmount, R.string.star_amount_placeholder, R.string.star_amount_placeholder_no_k),
                        repoId = it.repoId
                    )
                }

                this@RepositoriesViewmodel.repoItems.addAll(items)

                _state.value = _state.value!!.copy(
                    isLoading = false,
                    repos = this@RepositoriesViewmodel.repoItems
                )
            } catch (e: Exception) {
                _state.value = _state.value!!.copy(
                    isLoading = false,
                    isFailure = true
                )
            }
        }
    }

    fun onEndPage() {
        currentPage += 1
        searchForRepo()
    }

    fun refreshRepos(){
        currentPage = 0
        repoItems.clear()
        repoModels.clear()
        searchForRepo()
    }

    fun onItemClicked(repoId: Int) {
        val action = RepositoriesFragmentDirections.actionRepositoriesFragmentToRepoDetailFragment(repoModels.first {
            it.repoId == repoId
        })
        navController.navigate(action)
    }
}