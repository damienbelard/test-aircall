package com.nimade.testaircall.presentation_layer.screen.repositories

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.nimade.testaircall.R
import com.nimade.testaircall.databinding.FragmentRepositoriesBinding
import com.nimade.testaircall.presentation_layer.MainActivity
import com.nimade.testaircall.presentation_layer.listener.InfiniteScrollListener
import com.nimade.testaircall.presentation_layer.model.RepoItem
import com.nimade.testaircall.presentation_layer.ui_components.NotificationPanel
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

class RepositoriesFragment : Fragment(R.layout.fragment_repositories) {
    @FlowPreview
    @ExperimentalCoroutinesApi
    @Inject
    lateinit var vmFactory: RepositoriesViewmodel.Factory

    @FlowPreview
    @ExperimentalCoroutinesApi
    private lateinit var model: RepositoriesViewmodel

    private lateinit var groupAdapter: GroupAdapter<GroupieViewHolder>

    @ExperimentalCoroutinesApi
    private var notificationPanel: NotificationPanel? = null

    private var _binding: FragmentRepositoriesBinding? = null
    private val binding
        get() = _binding!!

    @ExperimentalCoroutinesApi
    @FlowPreview
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = FragmentRepositoriesBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).mainActivityComponent.inject(this)

        model = ViewModelProvider(this, vmFactory)[RepositoriesViewmodel::class.java]
        model.state.observe(viewLifecycleOwner, Observer { state ->
            renderState(state)
        })

        val layoutManager = LinearLayoutManager(context)
        groupAdapter = GroupAdapter()
        groupAdapter.setOnItemClickListener { item, _ ->
            val repoId = (item as RepoItem).repoId
            model.onItemClicked(repoId)
        }

        binding.apply {
            repositoriesRecyclerView.apply {
                adapter = groupAdapter
                this.layoutManager = layoutManager
                addOnScrollListener(object :
                    InfiniteScrollListener(layoutManager) {
                    override fun onLoadMore(current_page: Int) {
                        model.onEndPage()
                    }
                })
            }
            repositoriesSwipeRefreshLayout.setOnRefreshListener {
                model.refreshRepos()
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun renderState(state: RepositoriesState) {
        binding.repositoriesSwipeRefreshLayout.isRefreshing = state.isLoading
        groupAdapter.apply {
            clear()
            addAll(state.repos)
        }
        if (state.isFailure){
            if (notificationPanel == null) notificationPanel =
                NotificationPanel.makeModal(requireContext())
            notificationPanel!!
                .setPanel(
                    R.string.generic_error,
                    R.color.reddish,
                    false
                )
                .showPanel(
                    binding.root,
                    ConstraintLayout.LayoutParams(
                        ConstraintLayout.LayoutParams.MATCH_PARENT,
                        ConstraintLayout.LayoutParams.WRAP_CONTENT
                    ),
                    9f
                )
        } else notificationPanel?.hidePanel()
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}