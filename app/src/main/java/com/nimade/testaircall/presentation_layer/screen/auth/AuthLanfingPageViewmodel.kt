package com.nimade.testaircall.presentation_layer.screen.auth

import androidx.lifecycle.*
import androidx.navigation.NavController
import com.nimade.testaircall.R
import com.nimade.testaircall.domain_layer.GitUseCase
import com.nimade.testaircall.presentation_layer.streams.CoreStreams
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class AuthLanfingPageViewmodel(
    private val gitUseCase: GitUseCase,
    private val navController: NavController
) : ViewModel() {
    /* Factory for creating FeatureViewModel instances */
    class Factory @Inject constructor(
        private val gifUseCase: GitUseCase,
        private val navController: NavController
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return AuthLanfingPageViewmodel(
                gifUseCase,
                navController
            ) as T
        }
    }

    private val _state: MutableLiveData<AuthLandingPageState> = MutableLiveData()
    val state: LiveData<AuthLandingPageState>
        get() = _state

    init {
        _state.value = AuthLandingPageState()
        CoreStreams.isNetworkAvailable
            .asFlow()
            .flowOn(Dispatchers.Default)
            .distinctUntilChanged()
            .onEach { available ->
                if (available != null) {
                    _state.value = _state.value!!.copy(
                        networkAvailable = available == true
                    )
                }
            }
            .flowOn(Dispatchers.Main)
            .launchIn(GlobalScope)
    }

    @ExperimentalCoroutinesApi
    fun onLoginCodeReceived(code: String) {
        _state.value = _state.value!!.copy(
            isLoading = true,
            isLoginFailure = false
        )
        viewModelScope.launch(Dispatchers.Main) {
            val token = try {
                withContext(Dispatchers.IO) {
                    return@withContext gitUseCase.getToken(code).token
                }
            } catch (e: Exception) {
                _state.value = _state.value!!.copy(
                    isLoading = false,
                    isLoginFailure = true
                )
            }
            navController.navigate(R.id.action_authLandingPageFragment_to_repositoriesFragment)
        }
    }
}