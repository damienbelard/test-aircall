# Damien Belard - Android technical test

Here is the app I wrote for this test. It is not as good as I would have want to, since I lost too much time on the documentation (*** *** Github) for the auth then for the REST Api before realizing I should have go for the GraphQL Api.


## Architecture

I choose  MVVM which is the actual recommend architecture for Android and tried to follow the SOLID principale to make a CleanArchitecture

## Libraries

1. Dagger2 for dependency injection. Dagger 2 is the most complex but also the complete solution for DI on Android. It is also now recommended by Google through Hilt
2. Retrofit for HTTP calls as it recommended and satisfies all the need of this application
3. Navigation Component for the navigation

## Hard part

The hardest part was clearly the GitHub documentation The people who wrote it are clearly not using. It is not clear at all and lacks of examples.
This is specialy the case when it comes to authentication or the differencies between the REST Api and the GraphQL Api.
I lost a lot of time navigating trough this documentation to find what I needed (at least 1 full day cumulated) and made bad choices because the documentation was not clear.
Another hard part was to accept to deliver something which quality is not as good as  i woud like it to be

## Testing Strategy

Because of the lack of time, I chose to reduce test to minimum acceptable.
I unit tested the datasource and repository, which both contain important logic, like HTTP calls and model mapping.

With more time I could have unit test the viewmodels and make UI tests

## Purcentage of completion and what to improve on UX/UI

Based on what was asked, I'd say 80% are done. But to do the remaining, it requires to switch from the Rest Api to The GraphQL api. That work has been started on a dedicated branch (chores/switch-retrofit-to-appollo) but is not finished. It could probably be done in 1-2 days.

However this percentage drops if we take a look at what I wanted to do, like a better UX/UI with animation and a proper design system, UI tests, a splashscreen, a local DB for offline use, screen change animation, share functionality etc...


## What to improve on architecture

Nothing to say here, did my best on this part

