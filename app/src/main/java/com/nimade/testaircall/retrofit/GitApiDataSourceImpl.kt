package com.nimade.testaircall.retrofit

import com.nimade.testaircall.BuildConfig
import com.nimade.testaircall.data_layer.datasource.GitApiDataSource
import com.nimade.testaircall.data_layer.model.AuthLoginAccessTokenRequest
import com.nimade.testaircall.data_layer.model.AuthloginAccessTokenResponse
import com.nimade.testaircall.data_layer.model.SearchReposResponse

class GitApiDataSourceImpl(
    private val gitLoginService: GitLoginService,
    private val gitApiService: GitApiService
): GitApiDataSource {
    override suspend fun getAccessToken(code: String): AuthloginAccessTokenResponse = gitLoginService.getAccessToken(
        AuthLoginAccessTokenRequest(
            clientId = BuildConfig.CLIENT_ID,
            clientSecret = BuildConfig.CLIENT_SECRET,
            code = code
        )
    )

    override suspend fun getTopKotlinRepos(page: Int): SearchReposResponse = gitApiService.searchRepos(page)
}