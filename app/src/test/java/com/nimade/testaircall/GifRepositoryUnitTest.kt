package com.nimade.testaircall

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.nimade.testaircall.data_layer.GitRepositoryImpl
import com.nimade.testaircall.data_layer.mapper.AuthResponseModelMapper
import com.nimade.testaircall.data_layer.mapper.SearchResponseMapper
import com.nimade.testaircall.data_layer.model.AuthloginAccessTokenResponse
import com.nimade.testaircall.data_layer.model.SearchReposResponse
import com.nimade.testaircall.retrofit.GitApiDataSourceImpl
import com.nimade.testaircall.utils.JsonUtils
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class GifRepositoryUnitTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var gitRepositoryImpl: GitRepositoryImpl

    @MockK
    private lateinit var gitApiDataSourceImpl: GitApiDataSourceImpl

    private val authResponseModelMapper: AuthResponseModelMapper = AuthResponseModelMapper()
    private val searchResponseMapper: SearchResponseMapper = SearchResponseMapper()

    private lateinit var authloginAccessTokenResponse: AuthloginAccessTokenResponse
    private lateinit var searchReposResponse: SearchReposResponse

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        val gson = Gson()
        authloginAccessTokenResponse = gson.fromJson(
            JsonUtils.getJsonFromFile(
                javaClass.classLoader?.getResource("login_api_access_token_response_ok.json")
                    ?.openStream()!!
            )!!, AuthloginAccessTokenResponse::class.java
        )
        searchReposResponse = gson.fromJson(
            JsonUtils.getJsonFromFile(
                javaClass.classLoader?.getResource("data_api_sear_repo_response_ok.json")
                    ?.openStream()!!
            )!!, SearchReposResponse::class.java
        )
        gitRepositoryImpl = GitRepositoryImpl(
            authResponseModelMapper,
            searchResponseMapper,
            gitApiDataSourceImpl
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test mapper for access token`() {
        coEvery {
            gitApiDataSourceImpl.getAccessToken("code")
        } returns authloginAccessTokenResponse
        runBlocking {
            val authResponseModel = gitRepositoryImpl.getToken("code")
            assertEquals("e72e16c7e42f292c6912e7710c838347ae178b4a", authResponseModel.token)
        }

    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test mapper for search gif`() {
        coEvery {
            gitApiDataSourceImpl.getTopKotlinRepos(1)
        } returns searchReposResponse
        runBlocking {
            val repoModels = gitRepositoryImpl.getTopkotlinRepos(1)
            assertEquals(10, repoModels.size)
            assertEquals(
                14098069,
                repoModels.first().repoId
            )
        }

    }
}