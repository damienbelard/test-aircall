package com.nimade.testaircall.utils

import android.content.Context
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.nimade.testaircall.R
import com.nimade.testaircall.di.MainActivityScope
import javax.inject.Inject

@MainActivityScope
class ChromeCustomTabsUtils @Inject constructor(
    private val context: Context
) {
    fun openUrl(url: String) {
        CustomTabsIntent.Builder().setShowTitle(false)
            .setToolbarColor(ContextCompat.getColor(context, R.color.window_background_color))
            .build()
            .launchUrl(
                context,
                Uri.parse(url)
            )
    }
}