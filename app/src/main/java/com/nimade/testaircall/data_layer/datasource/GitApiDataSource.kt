package com.nimade.testaircall.data_layer.datasource

import com.nimade.testaircall.data_layer.model.AuthloginAccessTokenResponse
import com.nimade.testaircall.data_layer.model.SearchReposResponse

interface GitApiDataSource {
    suspend fun getAccessToken(code: String): AuthloginAccessTokenResponse
    suspend fun getTopKotlinRepos(page: Int): SearchReposResponse
}