package com.nimade.testaircall.presentation_layer.screen.auth

data class AuthLandingPageState(
    val isLoading: Boolean = false,
    val isLoginFailure: Boolean = false,
    val isLoginSuccess: Boolean = false,
    val networkAvailable: Boolean = true
)
