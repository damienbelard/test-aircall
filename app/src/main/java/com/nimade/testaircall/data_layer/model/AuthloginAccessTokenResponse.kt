package com.nimade.testaircall.data_layer.model

import com.google.gson.annotations.SerializedName

data class AuthloginAccessTokenResponse(
    @SerializedName("access_token")
    val token: String,
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("scope")
    val scope: String
)
