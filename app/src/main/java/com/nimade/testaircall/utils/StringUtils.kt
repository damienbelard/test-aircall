package com.nimade.testaircall.utils

import android.content.Context
import javax.inject.Inject


class StringUtils @Inject constructor(
    private val context: Context
) {
    fun parseAmount(starAmount: Int, placeholderResource: Int, placeholderNoKResource: Int): String {
        return starAmount.toString().let { repoStarAmount ->
            return@let if (repoStarAmount.length < 4) {
                context.resources.getString(
                    placeholderNoKResource,
                    repoStarAmount
                )
            }
            else {
                repoStarAmount.substring(0, repoStarAmount.length - 2).let {
                    context.resources.getString(
                        placeholderResource,
                        it.substring(0, it.length - 1), "${it[it.length - 1]}"
                    )
                }
            }
        }
    }
}