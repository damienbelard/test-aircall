package com.nimade.testaircall.presentation_layer.streams

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ConflatedBroadcastChannel

object CoreStreams {
    //Network Change Streams
    @ExperimentalCoroutinesApi
    val isNetworkAvailable = ConflatedBroadcastChannel<Boolean?>()
}