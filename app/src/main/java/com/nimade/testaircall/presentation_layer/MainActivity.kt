package com.nimade.testaircall.presentation_layer

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.navigation.findNavController
import com.nimade.testaircall.R
import com.nimade.testaircall.databinding.ActivityMainBinding
import com.nimade.testaircall.di.DaggerMainActivityComponent
import com.nimade.testaircall.di.MainActivityComponent
import com.nimade.testaircall.presentation_layer.broadcast_receiver.NetWorkChangedBroadcastReceiver
import com.nimade.testaircall.presentation_layer.streams.CoreStreams
import com.nimade.testaircall.presentation_layer.ui_components.NotificationPanel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

class MainActivity : AppCompatActivity() {

    @ExperimentalCoroutinesApi
    private var notificationPanel: NotificationPanel? = null

    lateinit var mainActivityComponent: MainActivityComponent
    private val netWorkChangedBroadcastReceiver = NetWorkChangedBroadcastReceiver()

    private lateinit var binding: ActivityMainBinding

    @InternalCoroutinesApi
    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.getRoot()
        setContentView(view)

        mainActivityComponent = DaggerMainActivityComponent.builder()
            .context(this)
            .navController(findNavController(R.id.root))
            .build()
        mainActivityComponent.inject(this)

        CoreStreams.isNetworkAvailable
            .asFlow()
            .flowOn(Dispatchers.Default)
            .distinctUntilChanged()
            .onEach { available ->
                if (notificationPanel == null) notificationPanel =
                    NotificationPanel.makeModal(this@MainActivity)
                if (available == true) notificationPanel!!.hidePanel()
                else notificationPanel!!
                    .setPanel(
                        R.string.network_error,
                        R.color.reddish,
                        false
                    )
                    .showPanel(
                        binding.mainActivityLayout,
                        ConstraintLayout.LayoutParams(
                            ConstraintLayout.LayoutParams.MATCH_PARENT,
                            ConstraintLayout.LayoutParams.WRAP_CONTENT
                        ),
                        9f
                    )

            }
            .flowOn(Dispatchers.Main)
            .launchIn(GlobalScope)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).registerDefaultNetworkCallback(
                object :
                    ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        super.onAvailable(network)
                        CoreStreams.isNetworkAvailable.offer(true)
                    }

                    override fun onLost(network: Network) {
                        super.onLost(network)
                        CoreStreams.isNetworkAvailable.offer(false)
                    }
                })
        } else {
            val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            registerReceiver(netWorkChangedBroadcastReceiver, filter)
        }
    }
}